Pod::Spec.new do |s|
  s.name         = "RNAppear2meePushNotifications"
  s.version      = "1.0.0"
  s.summary      = "Appear2mee notification management"
  s.description  = <<-DESC
                   Used direct Appear2mee push notifications to the Appear2mee SDK and pass other notifications to allow the developer to deal with them.
                   DESC
  s.homepage     = "https://2mee.com/"
  s.license      = "Copyright 2mee Ltd"
  # s.license      = { :type => "2meeLtd", :file => "FILE_LICENSE" }
  s.author             = { "author" => "2mee@2mee.com" }
  s.platforms    = { :ios => "7.0", :tvos => "9.0" }
  s.source       = { :bitbucket => "https://bitbucket.com/2meeltd/react-native-push-notifications-appear2mee.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m}"
  s.requires_arc = true
 # s.preserve_paths = 'ios/Appear2meeFramework/Appear2mee.framework'
 # s.xcconfig = { 'OTHER_LDFLAGS' => '-framework Appear2mee' }
  s.dependency "React"
  #s.dependency "others"

end
